#!/bin/bash
#
# OPAL Toolchain Build recipe for the GNU Scientific Library.
#
# https://www.gnu.org/software/gsl/
#
P=gsl
V=${GSL_VERSION:-2.7}

trap "otb_exit" EXIT

FNAME="$P-$V.tar.gz"
DOWNLOAD_URL="http://ftp.gnu.org/gnu/$P/${FNAME}"
SRC_FILE="${OTB_DOWNLOAD_DIR}/${FNAME}"

# download
mkdir -p "${OTB_DOWNLOAD_DIR}"
test -r "${SRC_FILE}" || curl -L --output "$_" "${DOWNLOAD_URL}" || exit ${OTB_ERR_DOWNLOAD}

# unpack
mkdir -p "${OTB_SRC_DIR}/$P" && cd "$_" || exit ${OTB_ERR_SYSTEM}
tar xvf "${SRC_FILE}" || exit ${OTB_ERR_UNTAR}

# configure
mkdir -p "${OTB_SRC_DIR}/$P/build" && cd "$_" || exit ${OTB_ERR_SYSTEM}
CFLAGS="-fPIC" "${OTB_SRC_DIR}/$P/$P-$V/configure" \
                --prefix="${OTB_PREFIX}" \
                --enable-shared \
                --enable-static \
               || exit ${OTB_ERR_CONFIGURE}

# compile & install
make -j ${NJOBS} || exit ${OTB_ERR_MAKE}
make install || exit ${OTB_ERR_INSTALL}

# Local Variables:
# mode: shell-script-mode
# sh-basic-offset: 8
# End:

