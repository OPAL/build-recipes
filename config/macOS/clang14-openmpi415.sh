export OTB_TOOLSET=clang
export OTB_COMPILER=clang
export OTB_COMPILER_VERSION=14.0.0
export OTB_MPI=openmpi
export OTB_MPI_VERSION=4.1.5

declare -a OTB_RECIPES=(
	050-build-cmake
	060-build-openmpi
	070-build-zlib
	080-build-hdf5
	090-build-gsl
	100-build-h5hut
	105-build-icu4c
	110-build-boost
	200-build-parmetis
	210-build-openblas
	220-build-trilinos
	240-build-MITHRA
	300-build-gtest)

declare -A OTB_SYMLINKS
